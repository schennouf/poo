#include "envelope.h"
#include <iostream>
#include <cstdlib>
#include <ostream>
#include <cstring> // for string
#include "address.h" // for address_t
using namespace std; // for string

envelope_t::envelope_t()
{
    this->priority = priority;
    this->recipient = recipient;
    this->sender = sender;
    this->height = 0;
    this->width = 0;
}

int envelope_t::get_height() const
{
    return this->height;
}

priority_t envelope_t::get_priority() const
{
    return this->priority;
}

address_t envelope_t::get_recipient() const
{
    return this->recipient;
}

address_t envelope_t::get_sender() const
{
    return this->sender;
}

int  envelope_t::get_width() const
{
    return this->width;
}

void  envelope_t::handle_e() const
{
    envelope_t *envelt;
    envelt = new envelope_t();
    cout<<envelt->get_sender()<<envelt->get_recipient()<<envelt->get_priority()<<envelt->get_height()<<"x"<<envelt->get_width()<<endl;
}

void envelope_t::handle_ec(int postal_code) const
{
    if(this->sender.get_postal_code() == postal_code)
    {
        handle_e();
    }

}

void  envelope_t::handle_ecge(int postal_code) const
{
    if(postal_code <= this->sender.get_postal_code())
    {
       handle_e();
    }

}

void  envelope_t::handle_ecgt(int postal_code) const
{
    
    if(postal_code < this->sender.get_postal_code())
    {
      handle_e();
    }

}

void  envelope_t::handle_ecle(int postal_code) const
{
    if(postal_code >= this->sender.get_postal_code())
    {
        handle_e();
    }
}

void  envelope_t::handle_eclt(int postal_code) const
{
    if(postal_code > this->sender.get_postal_code())
    {
        handle_e();
    }

}

void  envelope_t::handle_en(string name) const
{
    if(strstr(this->sender.get_name().c_str(),name.c_str()) != NULL)
    {
        handle_e();
    }

}

void envelope_t::set_priority(priority_t priority)
{
    this->priority = priority;
}

void  envelope_t::set_recipient(address_t recipient)
{
    this->recipient = recipient;
}

void  envelope_t::set_sender(address_t sender)
{
    this->sender = sender;
}
ostream& operator<<(ostream &os, const envelope_t &envelope)
{
    os<<envelope.get_sender() <<"->"<< envelope.get_recipient()<< envelope.get_priority()<<envelope.get_width()<<"x"<<envelope.get_height()<<endl;
    return os;
}
