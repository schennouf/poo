#include "company.h"
 #include <string> // for string
 #include <iostream>
 #include <cstdlib>
 #include <vector> // for vector
 #include "envelope.h" // for envelope_t
 using namespace std; // for string

 company_t::company_t()
 {
     this->name = "undefined";
     this->web = "undefined";
 }

 string company_t::get_name() const
 {
    return this->name;
 }

 string company_t::get_web() const
 {
    return this->web;
 }

void company_t::handle_e() const
{
    company_t *comp;
    comp = new company_t();
    cout<<comp->get_name()<<comp->get_web()<<endl;
}

void company_t::handle_i() const
{
    company_t comp;
    envelope_t env;
    cout<<comp.get_name()<<comp.get_web()<<env.get_sender()<<env.get_recipient()<<env.get_priority()<<env.get_height()<<"x"<<env.get_width()<<endl;
   
}

void company_t::handle_n() const
{
    company_t comp;
    cout<<comp.get_web()<<endl;
}

void company_t::handle_ec(int postal_code) const
{
    long unsigned int i;
    for(i=0;i<envelopes.size();i++)
    {
        handle_ec(postal_code);
    }
}

void company_t::handle_ecge(int postal_code) const
{
    long unsigned int i;
    for(i=0;i<envelopes.size();i++)
    {
        handle_ecge(postal_code);
    }
}

void company_t::handle_ecgt(int postal_code) const
{
    long unsigned int i;
    for(i=0;i<envelopes.size();i++)
    {
        handle_ecgt(postal_code);
    }
}

void company_t::handle_ecle(int postal_code) const
{
    long unsigned int i;
    for(i=0;i<envelopes.size();i++)
    {
        handle_ecle(postal_code);
    }
}

void company_t::handle_eclt(int postal_code) const
{
    long unsigned int i;
    for(i=0;i<envelopes.size();i++)
    {
        handle_eclt(postal_code);
    }
}

void company_t::handle_en(string name) const
{
   
}

void company_t::handle_w() const
{
    company_t comp;
    cout<<comp.get_web()<<endl;
}

envelope_t company_t::envelopes_at(int index) const
{
    return envelopes.at(index);
}

void company_t::envelopes_push_back(envelope_t envelope)
{
    envelopes.push_back(envelope);
}

int company_t::envelopes_size() const
{
    return envelopes.size();
}

void company_t::set_name(string name)
{
    this->name = name;
}

void company_t::set_web(string web)
{
    this->web = web;
}

ostream &operator<<(ostream &os, const company_t &company)
{
    int i;
    os<<company.get_name()<<company.get_web()<<endl;
    for(i=0;i<company.envelopes_size();i++)
    {
        os<<company.envelopes_at(i)<<endl;
    }
    return os;
}

