#include "str2i-error.h"
 #include <string>
 #include <sstream> // for string
 using namespace std; // for string

 str2i_error::str2i_error(string str)
 {
     ostringstream oss;
     oss<<"str2i_error: "<<str<<"to int";
     this->str = oss.str();
 }

str2i_error::~str2i_error() throw ()
{

}

const char * str2i_error::what() const throw ()
{
     return this->str.c_str();
}
 
