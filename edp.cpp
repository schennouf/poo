#include <cstdio>
#include <cstring>
#include <iostream>
#include "address.h"
#include "company.h"
#include "envelope-c4.h"
#include "envelope-dl.h"
#include "envelope.h"
#include "str2i-error.h"
#include "str2l-error.h"
#include "pugixml.cpp"
#include "pugixml.hpp"
using namespace pugi;
using namespace std;

/* Open Xml file */
int openxml(int argc, char *argv[], xml_node *racine) {
    /* Check if the number of parameters is correct*/
    if (argc != 2) {
        fprintf(stderr, "%s: Invalid number of arguments \n", argv[0]);
        exit(1);
    }
    xml_document doc;
    xml_parse_result resultat = doc.load_file(argv[1]);
    /* Check if the Xml path exist */
    if (!resultat) {
        fprintf(stderr, "I/O warning : failed to load external entity \"%s\" \n%s: Unable to parse the document \n", argv[1], argv[0]);
        exit(1);
    }
    *racine = doc.first_child();
    /* check if the Xml is empty */
    if (*racine == NULL) {
        fprintf(stderr, "I/O warning : failed to load external entity \"%s\" \n%s: Unable to parse the document \n", argv[1], argv[0]);
        exit(1);
    }
    return 0;
}


/* Panel fonction */
int panel(char* input, char* param, double num, char *argv[], xml_node racine) {
    /* check if in input there is the substring we look for*/
    if (strstr(input, "ecge") != NULL) {
        return 0;
    }
    if (strstr(input, "ecgt") != NULL) {
        num = strtod(param, NULL);
        return 0;
    }
    if (strstr(input, "ecle") != NULL) {
        num = strtod(param, NULL);
        return 0;
    }
    if (strstr(input, "eclt") != NULL) {
        num = strtod(param, NULL);
        return 0;
    }
    if (strstr(input, "ec") != NULL) {
        num = strtod(param, NULL);
        return 0;
    }
    if (strstr(input, "en") != NULL) {
        return 0;
    }
    if (strstr(input, "i") != NULL){
        return 0;
    } 

    if (strstr(input, "w") != NULL){
        return 0;
    } 
    if (strstr(input, "h") != NULL){
        cout<<"e: prints the envelopes \n"<<endl;
        cout<<"ec CODE: prints the envelopes with the sender postal code equal to CODE \n"<<endl;
        cout<<"ecge CODE: prints the envelopes with the sender postal code greater than or equal to CODE \n"<<endl;
        cout<<"ecgt CODE: prints the envelopes with the sender postal code greater than to CODE \n"<<endl;
        cout<<"ecle CODE: prints the envelopes with the sender postal code less than or equal to CODE \n"<<endl;
        cout<<"eclt CODE: prints the envelopes with the sender postal code less than CODE \n"<<endl;
        cout<<"h: prints this help \n"<<endl;
        cout<<"i: prints information about the company \n"<<endl;
        cout<<"n: prints the company name \n"<<endl;
        cout<<"q: quits EDP \n"<<endl;
        cout<<"v: prints the EDP Version help \n"<<endl;
        cout<<"w: prints the company web address \n"<<endl;
        return 0;
    } 
    if (strstr(input, "n") != NULL){
        return 0;
    }
    if (strstr(input, "v") != NULL){
        cout<<"EDP (Envelope Delivery Manager) 20220306 \n"<<"Copyright (C) 2022  Chennouf Sofiane \n"<<"Written by Chennouf Sofiane <schennouf@univ-pau.fr>.\n"<<endl;
        return 0;
    }
    if (strstr(input, "q") != NULL){
        cout<<"Goodbye ! \n"<<endl;
        exit(0);
    }
    else {
        /* in case of wrong command*/
        cerr<<argv[0]<<"Invalid command \n"<<endl;
        return 1;
    }

}

int main(int argc, char *argv[]) {
    /* Variables */
    /* Xml*/
    xml_document doc;
    xml_node racine;
    /* command */
    char input[15];
    char* param = NULL;
    char* ptrnumber;
    double number = 0;
    int i;
    int k = strlen(input);
  
    /* Open the file */
    openxml(argc, argv,&racine);

    /*Infinite Loop*/
    while (1) {
        fprintf(stdout, "EDP> ");
        /* Ask for the command, less than 25 characters*/
        fgets(input, 25, stdin);
        /* separate the commands and the parameters from the input*/
        if (k > 1) {
        for (i = 0; i < k; i++) {
            /* parcour the input array until find a space*/
            if(input[i] == ' ' || input[i] == '\0') {
                /* param is the rest of input after the space*/
                param = &input[i+1];
                param[strlen(param)-1] = '\0';
                /* convert param to a double named number*/
                number = strtod(param, &ptrnumber);
                }
            }
        }
        /* Open the panel*/
        panel(input, param, number, argv,racine);
        input[0] = '\0';
    }
   
return 0;
}
