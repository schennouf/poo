all: edp.o

address.o : address.cpp
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -c -o address.o address.cpp

company.o : company.cpp 
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml  --cflags` -c -o company.o company.cpp

envelope.o : envelope.cpp
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml  --cflags` -c -o envelope.o envelope.cpp

envelope-c4.o: envelope-c4.cpp 
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -c -o envelope-c4.o envelope-c4.cpp

envelope-dl.o: envelope-dl.cpp
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -c -o envelope-dl.o envelope-dl.cpp

str2i-error.o: str2i-error.cpp 
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -c -o str2i-error.o str2i-error.cpp

str2l-error.o: str2l-error.cpp 
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -c -o str2l-error.o str2l-error.cpp

edp.o : edp.cpp company.o address.o envelope.o envelope-c4.o envelope-dl.o str2i-error.o str2l-error.o
	g++ -std=c++98 -pedantic -Wall -Werror -g `pkg-config pugixml --cflags` -o edp.o edp.cpp company.o address.o envelope.o envelope-c4.o envelope-dl.o str2i-error.o str2l-error.o  `pkg-config pugixml --libs-only-L` `pkg-config pugixml --libs-only-l`
clean :
	rm -fv prog *.o *.out